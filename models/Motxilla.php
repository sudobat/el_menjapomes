<?php

class Motxilla {

	private $capacitat, $items;

	public function __construct( $capacitat = 6 )
	{
		$this->capacitat = $capacitat > 0 ? $capacitat : 6;
		$this->items = [];

		echo "S'ha creat una motxilla +".$this->capacitat."<br>";
	}

	public function getCapacitat()
	{
		return $this->capacitat;
	}

	public function guardar_item(Poma $poma)
	{
		# Comprovar si lamotxilla està plena
		if ( count($this->items) >= $this->capacitat )
		{
			echo "La motxilla està plena, treu una poma per guardar-ne una de nova<br>";
			return false;
		}

		# Guardar la poma
		array_push( $this->items, $poma );
		echo "Poma guardada a la motxilla, ".count($this->items)."/".$this->capacitat." ocupats<br>";

		return true;
	}

	public function treure_item($index)
	{
		# Comprovar que index >= 0 i index <= count($items)-1
		if ( $index < 0 || $index > count($this->items)-1 )
		{
			echo "No existeix aquest index als items de la motxilla<br>";
			return false;
		}

		# Guardem la poma
		$item = $this->items[$index];

		# Treiem la poma de la motxilla
		array_splice($this->items, $index, 1);

		# Retornem la Poma
		return $item;
	}

}