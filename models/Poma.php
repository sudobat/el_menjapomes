<?php

class Poma {

	private $x, $y, $saciament;

	public function __construct($saciament = 4, $x = 0, $y = 0)
	{
		$this->saciament = $saciament;
		$this->x = $x;
		$this->y = $y;

		echo "S'ha creat una poma +".$this->saciament." a la posició (".$this->x.", ".$this->y.")<br>";
	}

	public function getX()
	{
		return $this->x;
	}

	public function getY()
	{
		return $this->y;
	}

	public function getSaciament()
	{
		return $this->saciament;
	}
	
}
