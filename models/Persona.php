<?php

class Persona {

	private $nom, $x, $y, $gana, $gana_per_pas, $esta_viva, $gana_max;
	private $motxilla;

	public function __construct($nom = "Persona", $x = 0, $y = 0)
	{
		$this->nom = $nom;
		$this->x = $x;
		$this->y = $y;
		$this->gana = 0; // comencem sense gana i quan estem a gana_max de gana ens morim
		$this->gana_per_pas = 1; // cada passa que fem guanyem 1 de gana
		$this->esta_viva = true; // la creem viva per que sigui més divertit
		$this->gana_max = 20; // A partir de gana_max ens morim
		$this->motxilla = null; // Som pobres i no tenim motxilla

		echo "S'ha creat la persona ".$this->nom." a la posició (".$this->x.", ".$this->y.")<br>";
	}

	public function getNom()
	{
		return $this->nom;
	}

	public function getX()
	{
		return $this->x;
	}

	public function getY()
	{
		return $this->y;
	}

	public function getGana()
	{
		return $this->gana;
	}

	public function equipar_motxilla(Motxilla $motxilla)
	{
		$this->motxilla = $motxilla;

		echo $this->nom." s'ha equipat una motxilla +".$this->motxilla->getCapacitat()."<br>";

		return true;
	}

	public function caminar($direccio, $sentit)
	{
		# Comprovar que la direcció és vàlida ('x' o 'y')
		if ( $direccio != "x" && $direccio != "y" )
		{
			echo "La direcció del moviment no és vàlida<br>";
			return false;
		}

		# Comprovar que el sentit és vàlid (-1 o 1)
		if ( $sentit != -1 && $sentit != 1 )
		{
			echo "El sentit del moviment no és vàlid<br>";
			return false;
		}

		# Comprovar que la persona està viva
		if ( ! $this->esta_viva )
		{
			echo $this->nom." està morta i per tant no pot caminar<br>";
			return false;
		}

		# Moure a la persona
		/*if ( $direccio == "x" )
		{
			$this->x += $sentit;
		}
		else if ( $direccio == "y" )
		{
			$this->y += $sentit;
		}*/
		$this->{$direccio} += $sentit;
		echo $this->nom." s'ha desplaçat a (".$this->x.", ".$this->y.")<br>";

		# Incrementar-li la gana
		$this->gana += $this->gana_per_pas;
		echo $this->nom." té una gana de ".$this->gana."/".$this->gana_max."<br>";

		# Comprovar si estem morts
		if ( $this->gana >= $this->gana_max )
		{
			$this->esta_viva = false;
			echo $this->nom." s'ha mort de gana<br>";
		}

		return true;
	}

	public function recollir_item(array $pomes)
	{
		# Comprovar si tenim motxilles
		if ( ! $this->motxilla )
		{
			echo "No tens motxilla!<br>";
			return false;
		}

		# Comprovar si hi ha alguna poma a la meva casella
		foreach($pomes as $poma)
		{
			# Si està al mateix lloc que la persona, la guarda a la motxilla
			if (( $poma->getX() == $this->getX() ) && ( $poma->getY() == $this->getY() ))
			{
				return $this->motxilla->guardar_item($poma);
			}
		}

		# No hem trobat cap poma
		echo "No hi ha cap poma<br>";
		return false;
	}

	public function menjar($index)
	{
		# Treiem la poma de la motxilla
		$item = $this->motxilla->treure_item($index);

		# Comprovem que hem tret una poma
		if ( ! $item)
		{
			return false;
		}

		# Ens mengem la poma
		$this->gana -= $item->saciament;

		# Comprovem que la gana sigui >= 0
		if ($this->gana < 0) $this->gana = 0;

		echo $this->nom." s'ha menjat una poma i ara té ".$this->gana."/".$this->gana_max." gana<br>";

		return true;
	}

}