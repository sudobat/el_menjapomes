<?php

require_once('models/Persona.php');
require_once('models/Motxilla.php');
require_once('models/Poma.php');

echo "<h1>Benvinguts al joc del menja pomes</h1>";

# Creem una persona i la seva motxilla i l'hi equipem
$persona = new Persona("Adrià", 3, 4);
$motxilla = new Motxilla(4);

$persona->equipar_motxilla($motxilla);

# Creem una poma i la posem a l'array de pomes
$pomes = [];
$poma = new Poma(2, 4, 4);

array_push($pomes, $poma);